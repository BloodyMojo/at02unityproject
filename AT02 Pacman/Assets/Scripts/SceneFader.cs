using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;

public class SceneFader : MonoBehaviour
{
    public Image img;
    public AnimationCurve curve;

    private void Start()
    {
        StartCoroutine(FadeIn()); //Starts the FadeIn function at the start of the new scene
    }



    public void FadeTo(string scene)
    {
        StartCoroutine(FadeOut(scene)); //starts fadeout function and loads new scene
    }

    /// <summary>
    /// Changes the opacity of the image to none while following the curve
    /// </summary>
    /// <returns></returns>
    IEnumerator FadeIn()
    {
        float t = 1f;

        while (t > 0f)
        {
            t -= Time.deltaTime;
            float a = curve.Evaluate(t);
            img.color = new Color(0f, 0f, 0f, a);
            yield return 0;
        }
    }

    /// <summary>
    /// Changes the opacity of the image to full while following the curve
    /// </summary>
    IEnumerator FadeOut(string scene)
    {
        float t = 0f;

        while (t < 1f)
        {
            t += Time.deltaTime;
            float a = curve.Evaluate(t);
            img.color = new Color(0f, 0f, 0f, a);
            yield return 0;
        }

        SceneManager.LoadScene(scene);//Loads New scene
    }

}