using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MiniMap : MonoBehaviour
{
    public Transform player;

    public void LateUpdate()
    {
        Vector3 newPosition = player.position; //Gets the players position.
        newPosition.y = transform.position.y; //Keeps the camera position the same on the Y axis.
        transform.position = newPosition; //Makes the camera go to the new position.
    }
}
