using UnityEngine;
using UnityEngine.SceneManagement;

public class Button : MonoBehaviour
{
    public string levelToLoad = "MainLevel"; //Loads to new scene

    public SceneFader sceneFader;

    public void Play()
    {
        sceneFader.FadeTo(levelToLoad); //Plays sceneFader while loading into level
    }

    public void Quit() //Quits Application
    {
        Debug.Log("Exciting...");
        Application.Quit();
    }
}
